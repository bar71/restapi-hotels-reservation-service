toc.dat                                                                                             0000600 0004000 0002000 00000055230 14563403205 0014446 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP   )    )                |            hotels    10.23    16.2 R    _           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false         `           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         a           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         b           1262    32888    hotels    DATABASE     �   CREATE DATABASE hotels WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE hotels;
                postgres    false                     2615    2200    public    SCHEMA     2   -- *not* creating schema, since initdb creates it
 2   -- *not* dropping schema, since initdb creates it
                postgres    false         c           0    0    SCHEMA public    ACL     Q   REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    6         �            1259    32969    roomClasses    TABLE     d   CREATE TABLE public."roomClasses" (
    id integer NOT NULL,
    name character varying NOT NULL
);
 !   DROP TABLE public."roomClasses";
       public            postgres    false    6         �            1259    32967    RoomClasses_roomClassId_seq    SEQUENCE     �   CREATE SEQUENCE public."RoomClasses_roomClassId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."RoomClasses_roomClassId_seq";
       public          postgres    false    6    206         d           0    0    RoomClasses_roomClassId_seq    SEQUENCE OWNED BY     V   ALTER SEQUENCE public."RoomClasses_roomClassId_seq" OWNED BY public."roomClasses".id;
          public          postgres    false    205         �            1259    32911    cities    TABLE     a   CREATE TABLE public.cities (
    id integer NOT NULL,
    name character varying(52) NOT NULL
);
    DROP TABLE public.cities;
       public            postgres    false    6         �            1259    32909    cities_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.cities_id_seq;
       public          postgres    false    201    6         e           0    0    cities_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;
          public          postgres    false    200         �            1259    32999    hotelAdmins    TABLE     e   CREATE TABLE public."hotelAdmins" (
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL
);
 !   DROP TABLE public."hotelAdmins";
       public            postgres    false    6         �            1259    32932    hotels    TABLE       CREATE TABLE public.hotels (
    id integer NOT NULL,
    name character varying NOT NULL,
    "cityId" integer NOT NULL,
    stars integer NOT NULL,
    address character varying NOT NULL,
    description text NOT NULL,
    "distanceToCenter" integer NOT NULL
);
    DROP TABLE public.hotels;
       public            postgres    false    6         �            1259    32930    hotels_id_seq    SEQUENCE     �   CREATE SEQUENCE public.hotels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.hotels_id_seq;
       public          postgres    false    6    203         f           0    0    hotels_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.hotels_id_seq OWNED BY public.hotels.id;
          public          postgres    false    202         �            1259    33016    payments    TABLE     �   CREATE TABLE public.payments (
    id integer NOT NULL,
    "reservationId" integer NOT NULL,
    "checkPrice" integer NOT NULL,
    "paymentDate" timestamp without time zone NOT NULL
);
    DROP TABLE public.payments;
       public            postgres    false    6         �            1259    33014    payments_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.payments_id_seq;
       public          postgres    false    6    211         g           0    0    payments_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;
          public          postgres    false    210         �            1259    33024    reservations    TABLE     p  CREATE TABLE public.reservations (
    id integer NOT NULL,
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "roomId" integer NOT NULL,
    "startDate" date NOT NULL,
    "finalDate" date NOT NULL,
    "isAccepted" boolean NOT NULL,
    "isPaid" boolean NOT NULL,
    "totalPrice" integer NOT NULL,
    "isRejected" boolean DEFAULT false NOT NULL
);
     DROP TABLE public.reservations;
       public            postgres    false    6         �            1259    33022    reservations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.reservations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.reservations_id_seq;
       public          postgres    false    6    213         h           0    0    reservations_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.reservations_id_seq OWNED BY public.reservations.id;
          public          postgres    false    212         �            1259    32901    roles    TABLE     `   CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(15) NOT NULL
);
    DROP TABLE public.roles;
       public            postgres    false    6         �            1259    32899    roles_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public          postgres    false    6    199         i           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
          public          postgres    false    198         �            1259    32980    rooms    TABLE     �   CREATE TABLE public.rooms (
    id integer NOT NULL,
    "roomClassId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "costPerDay" integer NOT NULL,
    description text NOT NULL,
    "placesCount" integer NOT NULL
);
    DROP TABLE public.rooms;
       public            postgres    false    6         �            1259    32978    rooms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.rooms_id_seq;
       public          postgres    false    208    6         j           0    0    rooms_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.rooms_id_seq OWNED BY public.rooms.id;
          public          postgres    false    207         �            1259    32891    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    "encodedPassword" character varying NOT NULL,
    phone character varying NOT NULL
);
    DROP TABLE public.users;
       public            postgres    false    6         �            1259    32952 
   usersRoles    TABLE     c   CREATE TABLE public."usersRoles" (
    "userId" integer NOT NULL,
    "roleId" integer NOT NULL
);
     DROP TABLE public."usersRoles";
       public            postgres    false    6         �            1259    32889    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    197    6         k           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    196         �
           2604    32914 	   cities id    DEFAULT     f   ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);
 8   ALTER TABLE public.cities ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    200    201    201         �
           2604    32935 	   hotels id    DEFAULT     f   ALTER TABLE ONLY public.hotels ALTER COLUMN id SET DEFAULT nextval('public.hotels_id_seq'::regclass);
 8   ALTER TABLE public.hotels ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203         �
           2604    33019    payments id    DEFAULT     j   ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);
 :   ALTER TABLE public.payments ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211         �
           2604    33027    reservations id    DEFAULT     r   ALTER TABLE ONLY public.reservations ALTER COLUMN id SET DEFAULT nextval('public.reservations_id_seq'::regclass);
 >   ALTER TABLE public.reservations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    213    212    213         �
           2604    32904    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    198    199    199         �
           2604    32972    roomClasses id    DEFAULT     }   ALTER TABLE ONLY public."roomClasses" ALTER COLUMN id SET DEFAULT nextval('public."RoomClasses_roomClassId_seq"'::regclass);
 ?   ALTER TABLE public."roomClasses" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    206    206         �
           2604    32983    rooms id    DEFAULT     d   ALTER TABLE ONLY public.rooms ALTER COLUMN id SET DEFAULT nextval('public.rooms_id_seq'::regclass);
 7   ALTER TABLE public.rooms ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    208    208         �
           2604    32894    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    197    196    197         P          0    32911    cities 
   TABLE DATA           *   COPY public.cities (id, name) FROM stdin;
    public          postgres    false    201       2896.dat X          0    32999    hotelAdmins 
   TABLE DATA           <   COPY public."hotelAdmins" ("userId", "hotelId") FROM stdin;
    public          postgres    false    209       2904.dat R          0    32932    hotels 
   TABLE DATA           e   COPY public.hotels (id, name, "cityId", stars, address, description, "distanceToCenter") FROM stdin;
    public          postgres    false    203       2898.dat Z          0    33016    payments 
   TABLE DATA           T   COPY public.payments (id, "reservationId", "checkPrice", "paymentDate") FROM stdin;
    public          postgres    false    211       2906.dat \          0    33024    reservations 
   TABLE DATA           �   COPY public.reservations (id, "userId", "hotelId", "roomId", "startDate", "finalDate", "isAccepted", "isPaid", "totalPrice", "isRejected") FROM stdin;
    public          postgres    false    213       2908.dat N          0    32901    roles 
   TABLE DATA           )   COPY public.roles (id, name) FROM stdin;
    public          postgres    false    199       2894.dat U          0    32969    roomClasses 
   TABLE DATA           1   COPY public."roomClasses" (id, name) FROM stdin;
    public          postgres    false    206       2901.dat W          0    32980    rooms 
   TABLE DATA           g   COPY public.rooms (id, "roomClassId", "hotelId", "costPerDay", description, "placesCount") FROM stdin;
    public          postgres    false    208       2903.dat L          0    32891    users 
   TABLE DATA           J   COPY public.users (id, name, email, "encodedPassword", phone) FROM stdin;
    public          postgres    false    197       2892.dat S          0    32952 
   usersRoles 
   TABLE DATA           :   COPY public."usersRoles" ("userId", "roleId") FROM stdin;
    public          postgres    false    204       2899.dat l           0    0    RoomClasses_roomClassId_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."RoomClasses_roomClassId_seq"', 3, true);
          public          postgres    false    205         m           0    0    cities_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.cities_id_seq', 6, true);
          public          postgres    false    200         n           0    0    hotels_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.hotels_id_seq', 2, true);
          public          postgres    false    202         o           0    0    payments_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.payments_id_seq', 13, true);
          public          postgres    false    210         p           0    0    reservations_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.reservations_id_seq', 42, true);
          public          postgres    false    212         q           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 3, true);
          public          postgres    false    198         r           0    0    rooms_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.rooms_id_seq', 9, true);
          public          postgres    false    207         s           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 14, true);
          public          postgres    false    196         �
           2606    32977    roomClasses RoomClasses_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."roomClasses"
    ADD CONSTRAINT "RoomClasses_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."roomClasses" DROP CONSTRAINT "RoomClasses_pkey";
       public            postgres    false    206         �
           2606    32919    cities cities_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.cities DROP CONSTRAINT cities_pkey;
       public            postgres    false    201         �
           2606    33003    hotelAdmins hotelAdmins_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelAdmins_pkey" PRIMARY KEY ("userId");
 J   ALTER TABLE ONLY public."hotelAdmins" DROP CONSTRAINT "hotelAdmins_pkey";
       public            postgres    false    209         �
           2606    32940    hotels hotels_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT hotels_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.hotels DROP CONSTRAINT hotels_pkey;
       public            postgres    false    203         �
           2606    33021    payments payments_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_pkey;
       public            postgres    false    211         �
           2606    33029    reservations reservations_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.reservations DROP CONSTRAINT reservations_pkey;
       public            postgres    false    213         �
           2606    32906    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    199         �
           2606    32988    rooms rooms_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.rooms DROP CONSTRAINT rooms_pkey;
       public            postgres    false    208         �
           2606    32908    roles uniqueName 
   CONSTRAINT     M   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "uniqueName" UNIQUE (name);
 <   ALTER TABLE ONLY public.roles DROP CONSTRAINT "uniqueName";
       public            postgres    false    199         �
           2606    32924    users unique_email 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_email UNIQUE (email);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT unique_email;
       public            postgres    false    197         �
           2606    32947    users unique_phone 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_phone UNIQUE (phone);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT unique_phone;
       public            postgres    false    197         �
           2606    32956    usersRoles usersRoles_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "usersRoles_pkey" PRIMARY KEY ("userId", "roleId");
 H   ALTER TABLE ONLY public."usersRoles" DROP CONSTRAINT "usersRoles_pkey";
       public            postgres    false    204    204         �
           2606    32896    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    197         �
           2606    32941    hotels cityId_ref    FK CONSTRAINT     ~   ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT "cityId_ref" FOREIGN KEY ("cityId") REFERENCES public.cities(id) NOT VALID;
 =   ALTER TABLE ONLY public.hotels DROP CONSTRAINT "cityId_ref";
       public          postgres    false    2744    201    203         �
           2606    32994    rooms hotelId_ref    FK CONSTRAINT     u   ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);
 =   ALTER TABLE ONLY public.rooms DROP CONSTRAINT "hotelId_ref";
       public          postgres    false    208    2746    203         �
           2606    33009    hotelAdmins hotelId_ref    FK CONSTRAINT     }   ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);
 E   ALTER TABLE ONLY public."hotelAdmins" DROP CONSTRAINT "hotelId_ref";
       public          postgres    false    2746    203    209         �
           2606    33035    reservations hotelId_ref    FK CONSTRAINT     |   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);
 D   ALTER TABLE ONLY public.reservations DROP CONSTRAINT "hotelId_ref";
       public          postgres    false    213    2746    203         �
           2606    32962    usersRoles ref_roleId    FK CONSTRAINT     y   ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_roleId" FOREIGN KEY ("roleId") REFERENCES public.roles(id);
 C   ALTER TABLE ONLY public."usersRoles" DROP CONSTRAINT "ref_roleId";
       public          postgres    false    2740    199    204         �
           2606    32957    usersRoles ref_userId    FK CONSTRAINT     y   ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_userId" FOREIGN KEY ("userId") REFERENCES public.users(id);
 C   ALTER TABLE ONLY public."usersRoles" DROP CONSTRAINT "ref_userId";
       public          postgres    false    2738    204    197         �
           2606    33045    payments reservationId_ref    FK CONSTRAINT     �   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT "reservationId_ref" FOREIGN KEY ("reservationId") REFERENCES public.reservations(id) NOT VALID;
 F   ALTER TABLE ONLY public.payments DROP CONSTRAINT "reservationId_ref";
       public          postgres    false    213    2758    211         �
           2606    32989    rooms roomClassId_ref    FK CONSTRAINT     �   ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "roomClassId_ref" FOREIGN KEY ("roomClassId") REFERENCES public."roomClasses"(id);
 A   ALTER TABLE ONLY public.rooms DROP CONSTRAINT "roomClassId_ref";
       public          postgres    false    208    2750    206         �
           2606    33040    reservations roomId_ref    FK CONSTRAINT     y   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "roomId_ref" FOREIGN KEY ("roomId") REFERENCES public.rooms(id);
 C   ALTER TABLE ONLY public.reservations DROP CONSTRAINT "roomId_ref";
       public          postgres    false    213    2752    208         �
           2606    33004    hotelAdmins userId_ref    FK CONSTRAINT     z   ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);
 D   ALTER TABLE ONLY public."hotelAdmins" DROP CONSTRAINT "userId_ref";
       public          postgres    false    2738    197    209         �
           2606    33030    reservations userId_ref    FK CONSTRAINT     y   ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);
 C   ALTER TABLE ONLY public.reservations DROP CONSTRAINT "userId_ref";
       public          postgres    false    197    2738    213                                                                                                                                                                                                                                                                                                                                                                                2896.dat                                                                                            0000600 0004000 0002000 00000000137 14563403205 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        3	Могилев
1	Витебск
2	Минск
4	Гомель
5	Брест
6	Гродно
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                 2904.dat                                                                                            0000600 0004000 0002000 00000000030 14563403205 0014243 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	1
10	2
13	2
14	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        2898.dat                                                                                            0000600 0004000 0002000 00000000320 14563403205 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Бар55	1	5	ул. П.Бровки, 1-1	У нас Вы попробуете советское шампанское.	2500
2	Hotel Eridan	1	3	ул. Советская, 21/17	Хз, не был там.	0
\.


                                                                                                                                                                                                                                                                                                                2906.dat                                                                                            0000600 0004000 0002000 00000000005 14563403205 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2908.dat                                                                                            0000600 0004000 0002000 00000000373 14563403205 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        37	13	2	6	2024-02-25	2024-02-26	f	f	40	f
38	13	2	7	2024-02-25	2024-02-26	f	f	50	f
39	13	2	8	2024-02-25	2024-02-26	f	f	60	f
40	14	2	9	2024-02-28	2024-02-29	f	f	30	f
41	2	1	1	2024-02-14	2024-02-20	f	f	180	f
42	2	2	6	2024-02-14	2024-02-20	f	f	240	f
\.


                                                                                                                                                                                                                                                                     2894.dat                                                                                            0000600 0004000 0002000 00000000040 14563403205 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	CLIENT
2	ADMIN
3	MANAGER
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                2901.dat                                                                                            0000600 0004000 0002000 00000000066 14563403205 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	эконом
2	комфорт
3	премиум
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                          2903.dat                                                                                            0000600 0004000 0002000 00000000651 14563403205 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	1	30	В прихожей	1
2	2	1	50	В зале	2
3	2	2	70	Хороший номер	1
4	2	2	100	Тоже хороший, на два человека	2
5	3	2	130	ТООООП	1
6	1	2	40	Номер на одного  похуже	1
7	1	2	50	Номер на одного типа получше	1
8	1	2	60	Номер на одного типа еще лучше	1
9	1	2	30	Плохой номер на одного	1
\.


                                                                                       2892.dat                                                                                            0000600 0004000 0002000 00000001416 14563403205 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	Dmitriy	funnyfaceon12345@gmail.com	$2a$10$2ztpbJfgVPX5CdYtJZllFepqi3zMjehrzqCsdJdqsfGKke1tU7CLu	+375292139712
3	Vafledyu	vafl@mail.ru	$2a$10$0YvH45In3MRENYhR6Iu2z.4f3Zp6KLfKTb5evnaFylhDCkR3lF/Sq	+375291234567
4	Jan	jan@yandex.ru	$2a$10$YM/uXklW4l2EiFugkcCFGeeS6h1kmliiE9tIiV7KbtOfLBAxio/WW	+375334443322
9	aaa	jopa@asd	$2a$10$j2dVzxOX0zlMl/XxY/Etc.EFTjr0Q//J7LQJsHKrsCOuotZXn/f4u	3333
10	Vasya	bombila@mail.ru	$2a$10$ROWKi3Ge/yS4fwrucq2hr.WSokZQ8v.5VLhkc04aF0Kp1i3JpHB7u	+375295932133
12	Biba	biba@mail.com	$2a$10$efJcQinn6Co1PzvFf6uCBusYxOy9cICDJ7SmYiYRh/FyUQjxefs02	+375297777777
13	Kolya	kkk@mail.ru	$2a$10$z.eyw7Xf9qVH3HhQt8vJHechj.Hf1/xbsHWNEmiHFFZXJc2LvEB3C	+375295932144
14	Ян	jan@gmail.com	$2a$10$KUagA8ugj/NoYLyN9Y2eqO6w5SmFBadUsM3NpH3V1vFJb/58eNFjO	+375295932111
\.


                                                                                                                                                                                                                                                  2899.dat                                                                                            0000600 0004000 0002000 00000000100 14563403205 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	1
2	3
3	1
4	1
4	2
9	1
10	1
12	1
10	2
13	1
13	2
14	1
14	2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                restore.sql                                                                                         0000600 0004000 0002000 00000043111 14563403205 0015366 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.23
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE hotels;
--
-- Name: hotels; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE hotels WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';


ALTER DATABASE hotels OWNER TO postgres;

\connect hotels

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

--
-- Name: roomClasses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."roomClasses" (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public."roomClasses" OWNER TO postgres;

--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RoomClasses_roomClassId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public."RoomClasses_roomClassId_seq" OWNER TO postgres;

--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RoomClasses_roomClassId_seq" OWNED BY public."roomClasses".id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cities (
    id integer NOT NULL,
    name character varying(52) NOT NULL
);


ALTER TABLE public.cities OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.cities_id_seq OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- Name: hotelAdmins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."hotelAdmins" (
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL
);


ALTER TABLE public."hotelAdmins" OWNER TO postgres;

--
-- Name: hotels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hotels (
    id integer NOT NULL,
    name character varying NOT NULL,
    "cityId" integer NOT NULL,
    stars integer NOT NULL,
    address character varying NOT NULL,
    description text NOT NULL,
    "distanceToCenter" integer NOT NULL
);


ALTER TABLE public.hotels OWNER TO postgres;

--
-- Name: hotels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hotels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.hotels_id_seq OWNER TO postgres;

--
-- Name: hotels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hotels_id_seq OWNED BY public.hotels.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    id integer NOT NULL,
    "reservationId" integer NOT NULL,
    "checkPrice" integer NOT NULL,
    "paymentDate" timestamp without time zone NOT NULL
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.payments_id_seq OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- Name: reservations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservations (
    id integer NOT NULL,
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "roomId" integer NOT NULL,
    "startDate" date NOT NULL,
    "finalDate" date NOT NULL,
    "isAccepted" boolean NOT NULL,
    "isPaid" boolean NOT NULL,
    "totalPrice" integer NOT NULL,
    "isRejected" boolean DEFAULT false NOT NULL
);


ALTER TABLE public.reservations OWNER TO postgres;

--
-- Name: reservations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.reservations_id_seq OWNER TO postgres;

--
-- Name: reservations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservations_id_seq OWNED BY public.reservations.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(15) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: rooms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms (
    id integer NOT NULL,
    "roomClassId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "costPerDay" integer NOT NULL,
    description text NOT NULL,
    "placesCount" integer NOT NULL
);


ALTER TABLE public.rooms OWNER TO postgres;

--
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.rooms_id_seq OWNER TO postgres;

--
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_id_seq OWNED BY public.rooms.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    "encodedPassword" character varying NOT NULL,
    phone character varying NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: usersRoles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."usersRoles" (
    "userId" integer NOT NULL,
    "roleId" integer NOT NULL
);


ALTER TABLE public."usersRoles" OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- Name: hotels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels ALTER COLUMN id SET DEFAULT nextval('public.hotels_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- Name: reservations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations ALTER COLUMN id SET DEFAULT nextval('public.reservations_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: roomClasses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."roomClasses" ALTER COLUMN id SET DEFAULT nextval('public."RoomClasses_roomClassId_seq"'::regclass);


--
-- Name: rooms id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms ALTER COLUMN id SET DEFAULT nextval('public.rooms_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cities (id, name) FROM stdin;
\.
COPY public.cities (id, name) FROM '$$PATH$$/2896.dat';

--
-- Data for Name: hotelAdmins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."hotelAdmins" ("userId", "hotelId") FROM stdin;
\.
COPY public."hotelAdmins" ("userId", "hotelId") FROM '$$PATH$$/2904.dat';

--
-- Data for Name: hotels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hotels (id, name, "cityId", stars, address, description, "distanceToCenter") FROM stdin;
\.
COPY public.hotels (id, name, "cityId", stars, address, description, "distanceToCenter") FROM '$$PATH$$/2898.dat';

--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payments (id, "reservationId", "checkPrice", "paymentDate") FROM stdin;
\.
COPY public.payments (id, "reservationId", "checkPrice", "paymentDate") FROM '$$PATH$$/2906.dat';

--
-- Data for Name: reservations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservations (id, "userId", "hotelId", "roomId", "startDate", "finalDate", "isAccepted", "isPaid", "totalPrice", "isRejected") FROM stdin;
\.
COPY public.reservations (id, "userId", "hotelId", "roomId", "startDate", "finalDate", "isAccepted", "isPaid", "totalPrice", "isRejected") FROM '$$PATH$$/2908.dat';

--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
\.
COPY public.roles (id, name) FROM '$$PATH$$/2894.dat';

--
-- Data for Name: roomClasses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."roomClasses" (id, name) FROM stdin;
\.
COPY public."roomClasses" (id, name) FROM '$$PATH$$/2901.dat';

--
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms (id, "roomClassId", "hotelId", "costPerDay", description, "placesCount") FROM stdin;
\.
COPY public.rooms (id, "roomClassId", "hotelId", "costPerDay", description, "placesCount") FROM '$$PATH$$/2903.dat';

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, "encodedPassword", phone) FROM stdin;
\.
COPY public.users (id, name, email, "encodedPassword", phone) FROM '$$PATH$$/2892.dat';

--
-- Data for Name: usersRoles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."usersRoles" ("userId", "roleId") FROM stdin;
\.
COPY public."usersRoles" ("userId", "roleId") FROM '$$PATH$$/2899.dat';

--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RoomClasses_roomClassId_seq"', 3, true);


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cities_id_seq', 6, true);


--
-- Name: hotels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hotels_id_seq', 2, true);


--
-- Name: payments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payments_id_seq', 13, true);


--
-- Name: reservations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservations_id_seq', 42, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- Name: rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_id_seq', 9, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 14, true);


--
-- Name: roomClasses RoomClasses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."roomClasses"
    ADD CONSTRAINT "RoomClasses_pkey" PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: hotelAdmins hotelAdmins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelAdmins_pkey" PRIMARY KEY ("userId");


--
-- Name: hotels hotels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT hotels_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: reservations reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: rooms rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);


--
-- Name: roles uniqueName; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "uniqueName" UNIQUE (name);


--
-- Name: users unique_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_email UNIQUE (email);


--
-- Name: users unique_phone; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_phone UNIQUE (phone);


--
-- Name: usersRoles usersRoles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "usersRoles_pkey" PRIMARY KEY ("userId", "roleId");


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: hotels cityId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT "cityId_ref" FOREIGN KEY ("cityId") REFERENCES public.cities(id) NOT VALID;


--
-- Name: rooms hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: hotelAdmins hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: reservations hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: usersRoles ref_roleId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_roleId" FOREIGN KEY ("roleId") REFERENCES public.roles(id);


--
-- Name: usersRoles ref_userId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_userId" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- Name: payments reservationId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT "reservationId_ref" FOREIGN KEY ("reservationId") REFERENCES public.reservations(id) NOT VALID;


--
-- Name: rooms roomClassId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "roomClassId_ref" FOREIGN KEY ("roomClassId") REFERENCES public."roomClasses"(id);


--
-- Name: reservations roomId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "roomId_ref" FOREIGN KEY ("roomId") REFERENCES public.rooms(id);


--
-- Name: hotelAdmins userId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- Name: reservations userId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       