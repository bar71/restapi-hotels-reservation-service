--
-- PostgreSQL database dump
--

-- Dumped from database version 10.23
-- Dumped by pg_dump version 10.23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: roomClasses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."roomClasses" (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public."roomClasses" OWNER TO postgres;

--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RoomClasses_roomClassId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RoomClasses_roomClassId_seq" OWNER TO postgres;

--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RoomClasses_roomClassId_seq" OWNED BY public."roomClasses".id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cities (
    id integer NOT NULL,
    name character varying(52) NOT NULL
);


ALTER TABLE public.cities OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cities_id_seq OWNER TO postgres;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- Name: hotelAdmins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."hotelAdmins" (
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL
);


ALTER TABLE public."hotelAdmins" OWNER TO postgres;

--
-- Name: hotels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hotels (
    id integer NOT NULL,
    name character varying NOT NULL,
    "cityId" integer NOT NULL,
    stars integer NOT NULL,
    address character varying NOT NULL,
    description text NOT NULL,
    "distanceToCenter" integer NOT NULL
);


ALTER TABLE public.hotels OWNER TO postgres;

--
-- Name: hotels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hotels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hotels_id_seq OWNER TO postgres;

--
-- Name: hotels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hotels_id_seq OWNED BY public.hotels.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    id integer NOT NULL,
    "reservationId" integer NOT NULL,
    "checkPrice" integer NOT NULL,
    "paymentDate" timestamp without time zone NOT NULL
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payments_id_seq OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- Name: reservations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservations (
    id integer NOT NULL,
    "userId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "roomId" integer NOT NULL,
    "startDate" date NOT NULL,
    "finalDate" date NOT NULL,
    "isAccepted" boolean NOT NULL,
    "isPaid" boolean NOT NULL,
    "totalPrice" integer NOT NULL,
    "isRejected" boolean DEFAULT false NOT NULL
);


ALTER TABLE public.reservations OWNER TO postgres;

--
-- Name: reservations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservations_id_seq OWNER TO postgres;

--
-- Name: reservations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reservations_id_seq OWNED BY public.reservations.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(15) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: rooms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rooms (
    id integer NOT NULL,
    "roomClassId" integer NOT NULL,
    "hotelId" integer NOT NULL,
    "costPerDay" integer NOT NULL,
    description text NOT NULL,
    "placesCount" integer NOT NULL
);


ALTER TABLE public.rooms OWNER TO postgres;

--
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rooms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rooms_id_seq OWNER TO postgres;

--
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rooms_id_seq OWNED BY public.rooms.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    "encodedPassword" character varying NOT NULL,
    phone character varying NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: usersRoles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."usersRoles" (
    "userId" integer NOT NULL,
    "roleId" integer NOT NULL
);


ALTER TABLE public."usersRoles" OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- Name: hotels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels ALTER COLUMN id SET DEFAULT nextval('public.hotels_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- Name: reservations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations ALTER COLUMN id SET DEFAULT nextval('public.reservations_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: roomClasses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."roomClasses" ALTER COLUMN id SET DEFAULT nextval('public."RoomClasses_roomClassId_seq"'::regclass);


--
-- Name: rooms id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms ALTER COLUMN id SET DEFAULT nextval('public.rooms_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cities (id, name) FROM stdin;
1	Витебск
2	Минск
3	Могилев
4	Гомель
5	Брест
6	Гродно
\.


--
-- Data for Name: hotelAdmins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."hotelAdmins" ("userId", "hotelId") FROM stdin;
4	1
10	2
\.


--
-- Data for Name: hotels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hotels (id, name, "cityId", stars, address, description, "distanceToCenter") FROM stdin;
1	Бар55	1	3	ул. П.Бровки, 1-1	У нас Вы попробуете советское шампанское.	2500
2	Hotel Eridan	1	2	ул. Советская, 21/17	Хз, не был там.	0
\.


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payments (id, "reservationId", "checkPrice", "paymentDate") FROM stdin;
\.


--
-- Data for Name: reservations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservations (id, "userId", "hotelId", "roomId", "startDate", "finalDate", "isAccepted", "isPaid", "totalPrice", "isRejected") FROM stdin;
1	3	1	1	2024-02-08	2024-02-10	t	f	60	f
4	2	2	6	2024-02-15	2024-02-17	t	f	100	f
5	2	2	6	2024-02-21	2024-02-23	t	f	100	f
6	2	2	7	2024-02-18	2024-02-19	t	f	100	f
7	2	2	7	2024-02-21	2024-02-23	t	f	100	f
8	2	2	8	2024-02-19	2024-02-20	t	f	100	f
9	2	2	6	2024-02-23	2024-02-25	t	f	100	f
3	2	1	1	2024-02-16	2024-02-17	t	f	120	f
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
1	CLIENT
2	ADMIN
3	MANAGER
\.


--
-- Data for Name: roomClasses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."roomClasses" (id, name) FROM stdin;
1	эконом
2	комфорт
3	премиум
\.


--
-- Data for Name: rooms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rooms (id, "roomClassId", "hotelId", "costPerDay", description, "placesCount") FROM stdin;
1	1	1	30	В прихожей	1
2	2	1	50	В зале	2
3	2	2	70	Хороший номер	1
4	2	2	100	Тоже хороший, на два человека	2
5	3	2	130	ТООООП	1
6	1	2	40	Номер на одного  похуже	1
7	1	2	50	Номер на одного типа получше	1
8	1	2	60	Номер на одного типа еще лучше	1
9	1	2	30	Плохой номер на одного	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, "encodedPassword", phone) FROM stdin;
2	Dmitriy	funnyfaceon12345@gmail.com	$2a$10$2ztpbJfgVPX5CdYtJZllFepqi3zMjehrzqCsdJdqsfGKke1tU7CLu	+375292139712
3	Vafledyu	vafl@mail.ru	$2a$10$0YvH45In3MRENYhR6Iu2z.4f3Zp6KLfKTb5evnaFylhDCkR3lF/Sq	+375291234567
4	Jan	jan@yandex.ru	$2a$10$YM/uXklW4l2EiFugkcCFGeeS6h1kmliiE9tIiV7KbtOfLBAxio/WW	+375334443322
9	aaa	jopa@asd	$2a$10$j2dVzxOX0zlMl/XxY/Etc.EFTjr0Q//J7LQJsHKrsCOuotZXn/f4u	3333
10	Vasya	bombila@mail.ru	$2a$10$ROWKi3Ge/yS4fwrucq2hr.WSokZQ8v.5VLhkc04aF0Kp1i3JpHB7u	+375295932133
12	Biba	biba@mail.com	$2a$10$efJcQinn6Co1PzvFf6uCBusYxOy9cICDJ7SmYiYRh/FyUQjxefs02	+375297777777
\.


--
-- Data for Name: usersRoles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."usersRoles" ("userId", "roleId") FROM stdin;
2	1
2	3
3	1
4	1
4	2
9	1
10	1
12	1
10	2
\.


--
-- Name: RoomClasses_roomClassId_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RoomClasses_roomClassId_seq"', 3, true);


--
-- Name: cities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cities_id_seq', 6, true);


--
-- Name: hotels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hotels_id_seq', 2, true);


--
-- Name: payments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payments_id_seq', 1, false);


--
-- Name: reservations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservations_id_seq', 9, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 3, true);


--
-- Name: rooms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rooms_id_seq', 9, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 12, true);


--
-- Name: roomClasses RoomClasses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."roomClasses"
    ADD CONSTRAINT "RoomClasses_pkey" PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: hotelAdmins hotelAdmins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelAdmins_pkey" PRIMARY KEY ("userId");


--
-- Name: hotels hotels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT hotels_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: reservations reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: rooms rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);


--
-- Name: roles uniqueName; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "uniqueName" UNIQUE (name);


--
-- Name: users unique_email; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_email UNIQUE (email);


--
-- Name: users unique_phone; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unique_phone UNIQUE (phone);


--
-- Name: usersRoles usersRoles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "usersRoles_pkey" PRIMARY KEY ("userId", "roleId");


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: hotels cityId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT "cityId_ref" FOREIGN KEY ("cityId") REFERENCES public.cities(id) NOT VALID;


--
-- Name: rooms hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: hotelAdmins hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: reservations hotelId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "hotelId_ref" FOREIGN KEY ("hotelId") REFERENCES public.hotels(id);


--
-- Name: usersRoles ref_roleId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_roleId" FOREIGN KEY ("roleId") REFERENCES public.roles(id);


--
-- Name: usersRoles ref_userId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."usersRoles"
    ADD CONSTRAINT "ref_userId" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- Name: payments reservationId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT "reservationId_ref" FOREIGN KEY ("reservationId") REFERENCES public.reservations(id) NOT VALID;


--
-- Name: rooms roomClassId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rooms
    ADD CONSTRAINT "roomClassId_ref" FOREIGN KEY ("roomClassId") REFERENCES public."roomClasses"(id);


--
-- Name: reservations roomId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "roomId_ref" FOREIGN KEY ("roomId") REFERENCES public.rooms(id);


--
-- Name: hotelAdmins userId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."hotelAdmins"
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- Name: reservations userId_ref; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT "userId_ref" FOREIGN KEY ("userId") REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

