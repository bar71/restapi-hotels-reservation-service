FROM maven:3.9.6-eclipse-temurin-21-alpine
COPY . /app
WORKDIR /app
RUN mvn clean package
ENTRYPOINT ["java", "-jar", "target/hotels71-api.jar"]
EXPOSE 8080

