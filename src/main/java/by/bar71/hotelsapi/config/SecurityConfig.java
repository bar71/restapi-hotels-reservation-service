package by.bar71.hotelsapi.config;

import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.service.UserService;
import by.bar71.hotelsapi.web.filter.AppBlackListFilter;
import by.bar71.hotelsapi.web.filter.JwtAuthFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Collection;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthFilter jwtAuthFilter;

    private final AppBlackListFilter appBlackListFilter;

    private final UserService userService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(HttpMethod.OPTIONS).permitAll()
                        .requestMatchers(HttpMethod.GET, "/city", "/room-class").permitAll()
                        .requestMatchers("/auth/*", "/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**", "/room/filtered", "/hotel/filtered").permitAll()

                        .requestMatchers(HttpMethod.POST, "/reservation").hasRole("CLIENT")
                        .requestMatchers("/reservation/*/pay", "reservation/user/*").hasRole("CLIENT")

                        .requestMatchers(HttpMethod.POST, "/hotel-ban").hasRole("ADMIN")
                        .requestMatchers("/reservation/*/accept", "/reservation/*/reject", "/hotel/black-list", "reservation/hotel/*").hasRole("ADMIN")

                        .requestMatchers("/app-ban", "/user", "/hotel").hasRole("MAINTAINER")
                        .requestMatchers(HttpMethod.GET, "/reservation", "/hotel-ban/*").hasRole("MAINTAINER")
                        .requestMatchers(HttpMethod.POST, "/city", "/room", "/hotel").hasRole("MAINTAINER")
                        .requestMatchers(HttpMethod.PUT, "/city", "/room", "/hotel").hasRole("MAINTAINER")
                        .requestMatchers(HttpMethod.DELETE, "/city", "/room", "/hotel").hasRole("MAINTAINER")
                        .anyRequest().authenticated()
                )
                .sessionManagement(sess -> sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(appBlackListFilter, JwtAuthFilter.class)
        ;
        return httpSecurity.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                try {
                    User user = userService.selectUserByEmail(email);
                    return userDetails(user);
                } catch(IllegalStateException e) {
                    throw new UsernameNotFoundException(e.getMessage());
                }
            }
        };
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService());
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return daoAuthenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration cfg) throws Exception {
        return cfg.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public UserDetails userDetails(User u) {
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return u.getRoles();
            }

            @Override
            public String getPassword() {
                return u.getEncodedPassword();
            }

            @Override
            public String getUsername() {
                return u.getEmail();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }


}


