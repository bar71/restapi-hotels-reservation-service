package by.bar71.hotelsapi.web.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AuthEmailRequest {

    private String email;

    private String rawPassword;

}
