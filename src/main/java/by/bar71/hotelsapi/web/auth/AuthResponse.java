package by.bar71.hotelsapi.web.auth;

import by.bar71.hotelsapi.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AuthResponse {

    User user;

    String jwt;

}
