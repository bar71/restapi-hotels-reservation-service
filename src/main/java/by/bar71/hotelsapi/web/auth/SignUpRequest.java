package by.bar71.hotelsapi.web.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SignUpRequest {

    private String name;

    private String phone;

    private String email;

    private String rawPassword;

}
