package by.bar71.hotelsapi.web.filter;

import by.bar71.hotelsapi.service.AppBanService;
import by.bar71.hotelsapi.service.UserService;
import by.bar71.hotelsapi.service.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

@Component
public class AppBlackListFilter extends OncePerRequestFilter {

    @Autowired
    JwtService jwtService;

    @Autowired
    AppBanService appBanService;

    @Autowired
    UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {
        final String authHeader = request.getHeader("authorization");
        final String email;
        final String jwtToken;
        if(authHeader == null || !authHeader.startsWith("Bearer")) {
            filterChain.doFilter(request, response);
            return;
        }
        jwtToken = authHeader.substring(7);
        email = jwtService.extractUsername(jwtToken);
        if (appBanService.selectAppBanByUserId(userService.selectUserByEmail(email).getId()) != null) {
            throw new AccessDeniedException("Access denied");
        }
        filterChain.doFilter(request, response);
    }

}
