package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public List<User> selectUsers(@RequestParam(required = false) Integer hotelId,
                                  @RequestParam(required = false) String email) {
        return userService.selectUsers(hotelId, email);
    }

}
