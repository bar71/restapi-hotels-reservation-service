package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.City;
import by.bar71.hotelsapi.domain.HotelAdmin;
import by.bar71.hotelsapi.service.CityService;
import by.bar71.hotelsapi.service.HotelAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "hotel-admin")
public class HotelAdminController {

    @Autowired
    HotelAdminService hotelAdminService;

    @GetMapping
    public List<HotelAdmin> selectHotelAdmins(@RequestParam(required = false) Integer userId,
                                        @RequestParam(required = false) Integer hotelId) {
        return hotelAdminService.selectHotelAdmins(userId, hotelId);
    }

    @GetMapping("/{userId}")
    public HotelAdmin selectHotelAdminByUserId(@PathVariable Integer userId) {
        return hotelAdminService.selectHotelAdminByUserId(userId);
    }

    @PostMapping()
    public void insertHotelAdmin(@RequestBody HotelAdmin hotelAdmin) {
        hotelAdminService.insertHotelAdmin(hotelAdmin);
    }

    @DeleteMapping("/{userId}")
    public void deleteHotelAdmin(@PathVariable Integer userId) {
        hotelAdminService.deleteHotelAdmin(userId);
    }
}
