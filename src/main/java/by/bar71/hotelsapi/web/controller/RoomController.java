package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.Room;
import by.bar71.hotelsapi.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "room")
public class RoomController {

    @Autowired
    RoomService roomService;

    @GetMapping
    List<Room> selectRoom(@RequestParam(required = false) Integer id,
                          @RequestParam(required = false) Integer hotelId,
                          @RequestParam(required = false) Integer roomClassId,
                          @RequestParam(required = false) Integer placesCount) {
        return roomService.selectRoom(id, hotelId, roomClassId, placesCount);
    }

    @GetMapping("/{id}")
    Room selectRoomById(@PathVariable Integer id) {
        return roomService.selectRoomById(id);
    }

    @GetMapping("/filtered")
    public List<Room> selectFilteredRooms(@RequestParam Integer hotelId,
                                          @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
                                          @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date finalDate,
                                          @RequestParam Integer roomClassId,
                                          @RequestParam Integer placesCount) {
        return roomService.selectFilteredRooms(hotelId, startDate, finalDate, roomClassId, placesCount);
    }

    @PostMapping
    void insertRoom(@RequestBody Room room) {
        roomService.insertRoom(room);
    }

    @PutMapping
    void updateRoom(@RequestBody Room room) {
        roomService.updateRoom(room);
    }

    @DeleteMapping
    void deleteRoom(Integer id) {
        roomService.deleteRoom(id);
    }

}
