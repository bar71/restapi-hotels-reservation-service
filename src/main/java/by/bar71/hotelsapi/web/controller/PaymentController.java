package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.Payment;
import by.bar71.hotelsapi.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @GetMapping
    public List<Payment> selectPayments() {
        return paymentService.selectPayments();
    }

    @GetMapping("/{id}")
    public Payment selectPaymentById(@PathVariable Integer id) {
        return paymentService.selectPaymentById(id);
    }

    @PostMapping
    public void insertPayment(@RequestBody Payment payment) {
        paymentService.insertPayment(payment);
    }

    @DeleteMapping("/{id}")
    public void deletePayment(@PathVariable Integer id) {
        paymentService.deletePayment(id);
    }



}
