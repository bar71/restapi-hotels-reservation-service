package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.ban.HotelBan;
import by.bar71.hotelsapi.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "hotel")
public class HotelController {

    @Autowired
    HotelService hotelService;

    @GetMapping
    public List<Hotel> selectHotels(@RequestParam(required = false) Integer cityId,
                                    @RequestParam(required = false) Integer stars) {
        return hotelService.selectHotels(cityId, stars);
    }

    @GetMapping("/{id}")
    public Hotel selectHotelById(@PathVariable Integer id) {
        return hotelService.selectHotelById(id);
    }

    @GetMapping("/by-admin-id")
    public Hotel selectHotelByAdminId(
            @RequestHeader(name = "authorization") String authHeader) {
        return hotelService.selectHotelByAdminId(authHeader);
    }

    @GetMapping("/filtered")
    public List<Hotel> selectFilteredHotels(@RequestParam String cityName,
                                            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
                                            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date finalDate,
                                            @RequestParam Integer roomClassId,
                                            @RequestParam Integer placesCount) {
        return hotelService.selectFilteredHotels(cityName, startDate, finalDate, roomClassId, placesCount);
    }

    @GetMapping("/black-list")
    public List<HotelBan> selectHotelBans(
            @RequestHeader(name = "authorization") String authHeader) {
        return hotelService.selectHotelBans(authHeader);
    }

    @PostMapping()
    public void insertHotel(@RequestBody Hotel hotel) {
        hotelService.insertHotel(hotel);
    }

    @PutMapping()
    public void updateHotel(@RequestBody Hotel hotel) {
        hotelService.updateHotel(hotel);
    }

}
