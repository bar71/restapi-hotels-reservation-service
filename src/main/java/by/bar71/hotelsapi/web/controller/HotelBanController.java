package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.ban.HotelBan;
import by.bar71.hotelsapi.domain.ban.HotelBanRequestDto;
import by.bar71.hotelsapi.service.HotelBanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "hotel-ban")
public class HotelBanController {

    @Autowired
    HotelBanService hotelBanService;

    @GetMapping("/{hotelId}")
    List<HotelBan> selectHotelBansByHotelId(@PathVariable Integer hotelId) {
        return hotelBanService.selectHotelBansByHotelId(hotelId);
    }

    @PostMapping
    void insertHotelBan(
            @RequestHeader(name = "authorization") String authHeader,
            @RequestBody HotelBanRequestDto hotelBanRequestDto) {
        hotelBanService.insertHotelBan(hotelBanRequestDto, authHeader);
    }

    @DeleteMapping
    void deleteHotelBan(
            @RequestHeader(name = "authorization") String authHeader,
            @RequestParam Integer userId) {
        hotelBanService.deleteHotelBan(userId, authHeader);
    }

}
