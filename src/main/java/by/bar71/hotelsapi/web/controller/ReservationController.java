package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.*;
import by.bar71.hotelsapi.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "reservation")
public class ReservationController {

    @Autowired
    ReservationService reservationService;

    @GetMapping
    public List<Reservation> selectReservations() {
        return reservationService.selectReservations();
    }

    @GetMapping("/{id}")
    Reservation selectReservationById(@PathVariable Integer id) {
        return reservationService.selectReservationById(id);
    }

    @GetMapping("/user/waiting-accept")
    public List<Reservation> selectUserWaitingAcceptReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectUserWaitingAcceptReservations(authHeader);
    }

    @GetMapping("/user/waiting-payment")
    public List<Reservation> selectUserWaitingPaymentReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectUserWaitingPaymentReservations(authHeader);
    }

    @GetMapping("/user/paid")
    public List<Reservation> selectUserPaidReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectUserPaidReservations(authHeader);
    }

    @GetMapping("/hotel/waiting-accept")
    public List<Reservation> selectHotelWaitingAcceptReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectHotelWaitingAcceptReservations(authHeader);
    }

    @GetMapping("/hotel/waiting-payment")
    public List<Reservation> selectHotelWaitingPaymentReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectHotelWaitingPaymentReservations(authHeader);
    }

    @GetMapping("/hotel/paid")
    public List<Reservation> selectHotelPaidReservations(
            @RequestHeader(name = "authorization") String authHeader) {
        return reservationService.selectHotelPaidReservations(authHeader);
    }

    @PostMapping
    void insertReservation(@RequestHeader(name = "authorization") String authHeader,
                           @RequestBody InsertReservationRequestDto insertReservationRequestDto) {
        reservationService.insertReservation(
                insertReservationRequestDto, authHeader
        );
    }

    @PutMapping("/{id}/accept")
    void accept(
            @RequestHeader(name = "authorization") String authHeader,
            @PathVariable Integer id) {
        reservationService.accept(id, authHeader);
    }

    @PutMapping("/{id}/reject")
    void reject(
            @RequestHeader(name = "authorization") String authHeader,
            @PathVariable Integer id) {
        reservationService.reject(id, authHeader);
    }

    @PutMapping("/{id}/pay")
    void pay(
            @RequestHeader(name = "authorization") String authHeader,
            @PathVariable Integer id) {
        reservationService.pay(id, authHeader);
    }

    @DeleteMapping("/{id}")
    void deleteReservation(@PathVariable Integer id) {
        reservationService.deleteReservation(id);
    }

}
