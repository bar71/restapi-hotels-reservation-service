package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.City;
import by.bar71.hotelsapi.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "city")
public class CityController {

    @Autowired
    CityService cityService;

    @GetMapping
    public List<City> selectCities(@RequestParam(required = false) Integer id,
                                      @RequestParam(required = false) String name) {
        return cityService.selectCities(id, name);
    }

    @GetMapping("/{id}")
    public City selectCityById(@PathVariable Integer id) {
        return cityService.selectCityById(id);
    }

    @PostMapping()
    public void insertCity(@RequestBody String name) {
        cityService.insertCity(name);
    }

    @PutMapping()
    public void updateCity(@RequestBody City city) {
        cityService.updateCity(city);
    }

}
