package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.service.UserService;
import by.bar71.hotelsapi.web.auth.AuthEmailRequest;
import by.bar71.hotelsapi.web.auth.AuthPhoneRequest;
import by.bar71.hotelsapi.web.auth.AuthResponse;
import by.bar71.hotelsapi.web.auth.SignUpRequest;
import by.bar71.hotelsapi.service.JwtService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "auth")
@AllArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    private final JwtService jwtService;

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/email")
    public AuthResponse authByEmail(@RequestBody AuthEmailRequest authEmailRequest) {
        final UserDetails user = userDetailsService.loadUserByUsername(authEmailRequest.getEmail());
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authEmailRequest.getEmail(), authEmailRequest.getRawPassword())
        );
        return new AuthResponse(userService.selectUserByEmail(authEmailRequest.getEmail()), jwtService.generateToken(user));

    }

    @PostMapping("/phone")
    public AuthResponse authByPhone(@RequestBody AuthPhoneRequest authPhoneRequest) {
        final User u = userService.selectUserByPhone(authPhoneRequest.getPhone());
        final UserDetails user = userDetailsService.loadUserByUsername(u.getEmail());
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(u.getEmail(), authPhoneRequest.getRawPassword())
        );
        return new AuthResponse(userService.selectUserByEmail(u.getEmail()), jwtService.generateToken(user));
    }

    @PostMapping("/signup")
    public AuthResponse signup(@RequestBody SignUpRequest signUpRequest) {
        String rawPassword = signUpRequest.getRawPassword();
        User user = new User(signUpRequest, passwordEncoder);
        userService.insertUser(user);
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), rawPassword)
        );
        return new AuthResponse(userService.selectUserByEmail(user.getEmail()), jwtService.generateToken(userDetails));

    }

}
