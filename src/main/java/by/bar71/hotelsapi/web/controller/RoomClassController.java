package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.RoomClass;
import by.bar71.hotelsapi.service.RoomClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "room-class")
public class RoomClassController {

    @Autowired
    RoomClassService roomClassService;

    @GetMapping
    public List<RoomClass> selectAllRoomClasses() {
        return roomClassService.selectAllRoomClasses();
    }

    @GetMapping("/{id}")
    public RoomClass selectRoomClassById(@PathVariable Integer id) {
        return roomClassService.selectRoomClassById(id);
    }

}
