package by.bar71.hotelsapi.web.controller;

import by.bar71.hotelsapi.domain.ban.*;
import by.bar71.hotelsapi.service.AppBanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "app-ban")
public class AppBanController {

    @Autowired
    AppBanService appBanService;

    @GetMapping("")
    List<AppBan> selectAppBans() {
        return appBanService.selectAppBans();
    }

    @PostMapping
    void insertAppBan(
            @RequestHeader(name = "authorization") String authHeader,
            @RequestBody AppBanRequestDto appBanRequestDto) {
        appBanService.insertAppBan(appBanRequestDto, authHeader);
    }

    @DeleteMapping
    void deleteAppBan(@RequestParam Integer userId) {
        appBanService.deleteAppBan(userId);
    }

}
