package by.bar71.hotelsapi.domain;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity(name = "reservations")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "hotelId")
    private Hotel hotel;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "roomId")
    private Room room;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "finalDate")
    private Date finalDate;

    @Column(name = "isPaid")
    private boolean isPaid;

    @Column(name = "isAccepted")
    private boolean isAccepted;

    @Column(name = "totalPrice")
    private Integer totalPrice;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "reservation")
    private Payment payment;

}
