package by.bar71.hotelsapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Entity(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "checkPrice")
    private Integer checkPrice;

    @Column(name = "paymentDateTime")
    private Date paymentDateTime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reservationId")
    @ToString.Exclude
    @JsonIgnore
    private Reservation reservation;
}
