package by.bar71.hotelsapi.domain.ban;

import lombok.*;

@NoArgsConstructor
@Setter
@Getter
@ToString
@AllArgsConstructor
public class AppBanRequestDto {

    private String email;

    private String reason;

}
