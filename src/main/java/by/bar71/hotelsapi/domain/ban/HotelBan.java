package by.bar71.hotelsapi.domain.ban;

import by.bar71.hotelsapi.domain.User;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@Entity(name = "hotelsBans")
public class HotelBan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "judgeId")
    private User judge;

    @Column(name = "hotelId")
    private Integer hotelId;

    @Column(name = "reason")
    private String reason;

    @Column(name = "banDate")
    private Date banDate;

}
