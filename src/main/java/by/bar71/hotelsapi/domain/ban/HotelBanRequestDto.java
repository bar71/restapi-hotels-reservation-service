package by.bar71.hotelsapi.domain.ban;

import lombok.*;

@NoArgsConstructor
@Setter
@Getter
@ToString
@AllArgsConstructor
public class HotelBanRequestDto {

    private Integer userId;

    private String reason;

}
