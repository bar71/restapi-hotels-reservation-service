package by.bar71.hotelsapi.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "hotelAdmins")
public class HotelAdmin {

    @Id
    @Column(name = "userId")
    private Integer userId;

    @Column(name = "hotelId")
    private Integer hotelId;

}
