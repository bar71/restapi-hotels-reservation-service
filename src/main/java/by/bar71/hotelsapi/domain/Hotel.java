package by.bar71.hotelsapi.domain;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.type.descriptor.java.PrimitiveByteArrayJavaType;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "hotels")
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "image")
    private String image;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cityId")
    private City city;

    @Column(name = "stars")
    private Integer stars;

    @Column(name = "address")
    private String address;

    @Column(name = "description")
    private String description;

    @Column(name = "distanceToCenter")
    private Integer distanceToCenter;

}
