package by.bar71.hotelsapi.domain;

import by.bar71.hotelsapi.web.auth.SignUpRequest;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Entity(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "encodedPassword")
    private String encodedPassword;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usersRoles",
            joinColumns = {
                    @JoinColumn(name = "userId")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "roleId")
            }
    )
    private List<Role> roles = new LinkedList<>();

    public User(SignUpRequest signUpRequest, PasswordEncoder passwordEncoder) {
        email = signUpRequest.getEmail();
        name = signUpRequest.getName();
        phone = signUpRequest.getPhone();
        this.setEncodedPassword(passwordEncoder.encode(signUpRequest.getRawPassword()));
    }

}
