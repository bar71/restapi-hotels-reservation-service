package by.bar71.hotelsapi.domain;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class InsertReservationRequestDto {

    private Integer userId;

    private Integer hotelId;

    private Integer roomId;

    private Date startDate;

    private Date finalDate;

    private Integer totalPrice;

}
