package by.bar71.hotelsapi.domain;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Table;
import org.springframework.boot.autoconfigure.web.WebProperties;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor
@Builder
@Entity(name = "cities")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

}
