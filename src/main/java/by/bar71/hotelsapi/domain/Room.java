package by.bar71.hotelsapi.domain;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "roomClassId")
    private RoomClass roomClass;

    @Column(name = "hotelId")
    private Integer hotelId;

    @Column(name = "costPerDay")
    private Integer costPerDay;

    @Column(name = "description")
    private String description;

    @Column(name = "placesCount")
    private Integer placesCount;

}
