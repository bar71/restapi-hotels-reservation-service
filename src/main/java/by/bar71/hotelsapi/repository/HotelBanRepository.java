package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.ban.HotelBan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HotelBanRepository extends JpaRepository<HotelBan, Integer> {

    HotelBan findByUserIdAndHotelId(Integer userId, Integer hotelId);

    List<HotelBan> findByHotelId(Integer hotelId);

    void deleteByUserIdAndHotelId(Integer userId, Integer hotelId);

}
