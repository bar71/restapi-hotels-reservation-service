package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.InsertReservationRequestDto;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    @Modifying
    @Query(
            value = "        INSERT INTO reservations (\"userId\", \"hotelId\", \"roomId\", \"startDate\", \"finalDate\", \"isAccepted\", \"isPaid\", \"totalPrice\")\n" +
                    "        VALUES (:#{#irrd.userId}, :#{#irrd.hotelId}, :#{#irrd.roomId}, (:#{#irrd.startDate}), (:#{#irrd.finalDate}), false, false, :#{#irrd.totalPrice})",
            nativeQuery = true
    )
    void insertReservationByRequest(@Param(value = "irrd") InsertReservationRequestDto irrd);

    List<Reservation> findByUserAndIsAcceptedIsFalse(User user);

    List<Reservation> findByUserAndIsAcceptedIsTrueAndIsPaidIsFalse(User user);

    List<Reservation> findByUserAndIsPaidIsTrue(User user);

    List<Reservation> findByHotelAndIsAcceptedIsFalse(Hotel hotel);

    List<Reservation> findByHotelAndIsAcceptedIsTrueAndIsPaidIsFalse(Hotel hotel);

    List<Reservation> findByHotelAndIsPaidIsTrue(Hotel hotel);

    @Modifying
    void deleteByUserAndIsPaidIsFalse(User user);

}
