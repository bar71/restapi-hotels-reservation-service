package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.function.Predicate;

public interface UserRepository extends JpaRepository<User, Integer>, QuerydslPredicateExecutor<User> {

    User findByEmail(String email);

    User findByPhone(String phone);

    @Query(
            value = "INSERT INTO \"usersRoles\" (\"userId\", \"roleId\") VALUES (?1, ?2)",
            nativeQuery = true
    )
    @Modifying
    void insertUserRole(Integer userId, Integer roleId);

    @Query(
            value = "DELETE FROM \"usersRoles\" WHERE \"userId\" = ?1",
            nativeQuery = true
    )
    @Modifying
    void deleteUserRoles(Integer userId);

    @Query(
            value = "        SELECT * FROM reservations\n" +
                    "        WHERE reservations.\"userId\" = ?1 AND reservations.\"isPaid\" = true",
            nativeQuery = true
    )
    Hotel selectHotelByAdminId(Integer id);

    @Query(
            value = "        SELECT * FROM users\n" +
                    "            JOIN \"hotelAdmins\" ON id = \"hotelAdmins\".\"userId\"\n" +
                    "        WHERE \"hotelAdmins\".\"hotelId\" = ?1",
            nativeQuery = true
    )
    List<User> selectAdminsOfHotel(Integer hotelId);

    @Query(
            value = "        DELETE FROM \"usersRoles\"\n" +
                    "        WHERE \"userId\" = ?1 AND \"roleId\" = ?2",
            nativeQuery = true
    )
    @Modifying
    void deleteUserRole(Integer userId, Integer roleId);
}
