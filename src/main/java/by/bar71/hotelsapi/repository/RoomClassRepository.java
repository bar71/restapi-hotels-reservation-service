package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.RoomClass;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoomClassRepository extends JpaRepository<RoomClass, Integer> {
}
