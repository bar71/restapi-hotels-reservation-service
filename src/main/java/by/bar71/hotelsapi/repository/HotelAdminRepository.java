package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.HotelAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface HotelAdminRepository extends JpaRepository<HotelAdmin, Integer>, QuerydslPredicateExecutor<HotelAdmin> {

    HotelAdmin findByUserId(Integer userId);

}
