package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);

}
