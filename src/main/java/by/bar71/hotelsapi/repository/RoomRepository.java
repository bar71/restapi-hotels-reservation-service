package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.Room;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Integer>, QuerydslPredicateExecutor<Room> {

    //List<Room> selectRooms(Integer id, Integer hotelId, Integer roomClassId, Integer placesCount);

    @Query(
            value = "        SELECT DISTINCT rooms.* FROM rooms\n" +
                    "             LEFT JOIN public.reservations reserv on rooms.id = reserv.\"roomId\"\n" +
                    "        WHERE rooms.\"hotelId\" = :hotelId AND \"roomClassId\" = :roomClassId AND \"placesCount\" = :placesCount\n" +
                    "        GROUP BY rooms.id, rooms.\"placesCount\", rooms.\"costPerDay\", rooms.\"roomClassId\", rooms.\"hotelId\"\n" +
                    "        HAVING SUM(CASE\n" +
                    "                       WHEN\n" +
                    "                        (reserv.\"startDate\" <= :startDate AND reserv.\"finalDate\" > :startDate)\n" +
                    "                               OR\n" +
                    "                           (reserv.\"startDate\" > :startDate AND :finalDate > reserv.\"startDate\") THEN 1\n" +
                    "                       ELSE 0\n" +
                    "            END\n" +
                    "               ) = 0",
            nativeQuery = true
    )
    List<Room> selectFilteredRooms(
            @Param(value = "hotelId") Integer hotelId,
            @Param(value = "startDate") Date startDate,
            @Param(value = "finalDate") Date finalDate,
            @Param(value = "roomClassId") Integer roomClassId,
            @Param(value = "placesCount") Integer placesCount);
}
