package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {

}
