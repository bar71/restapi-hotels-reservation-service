package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.ban.AppBan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AppBanRepository extends JpaRepository<AppBan, Integer> {

    AppBan findByUserId(Integer userId);

    @Modifying
    void deleteByUserId(Integer userId);

}
