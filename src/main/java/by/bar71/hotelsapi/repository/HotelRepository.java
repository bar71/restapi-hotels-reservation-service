package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface HotelRepository extends JpaRepository<Hotel, Integer>, QuerydslPredicateExecutor<Hotel> {

    @Query(
            value = "SELECT DISTINCT hotels.* FROM hotels\n" +
                    "  INNER JOIN public.rooms r on hotels.id = r.\"hotelId\"\n" +
                    "  LEFT JOIN public.reservations reserv on r.id = reserv.\"roomId\"\n" +
                    "WHERE \"cityId\" = (SELECT id FROM cities WHERE name = :cityName) AND \"roomClassId\" = :roomClassId AND \"placesCount\" = :placesCount\n" +
                    "GROUP BY hotels.id, hotels.name, hotels.\"cityId\", hotels.stars, hotels.address, hotels.description, hotels.\"distanceToCenter\", r.id\n" +
                    "HAVING SUM(CASE\n" +
                    "               WHEN\n" +
                    "                (reserv.\"startDate\" <= :startDate AND reserv.\"finalDate\" > :startDate)\n" +
                    "                       OR\n" +
                    "                   (reserv.\"startDate\" > :startDate AND :finalDate > reserv.\"startDate\") THEN 1\n" +
                    "               ELSE 0\n" +
                    "    END\n" +
                    "       ) = 0",
            nativeQuery = true
    )
    List<Hotel> selectFilteredHotels(
            @Param(value = "cityName") String cityName,
            @Param(value = "startDate") Date startDate,
            @Param(value = "finalDate") Date finalDate,
            @Param(value = "roomClassId") Integer roomClassId,
            @Param(value = "placesCount") Integer placesCount);

}
