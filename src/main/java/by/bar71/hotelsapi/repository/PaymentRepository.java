package by.bar71.hotelsapi.repository;

import by.bar71.hotelsapi.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    Payment findByReservationId(Integer id);

}
