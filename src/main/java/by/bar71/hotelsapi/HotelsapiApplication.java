package by.bar71.hotelsapi;

import by.bar71.hotelsapi.domain.*;
import by.bar71.hotelsapi.repository.*;
import by.bar71.hotelsapi.service.*;
import by.bar71.hotelsapi.service.implementations.CityServiceImpl;
import by.bar71.hotelsapi.service.implementations.RoomClassServiceImpl;
import by.bar71.hotelsapi.service.implementations.UserServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Configuration
@SpringBootApplication
public class HotelsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelsapiApplication.class, args);
	}

}
