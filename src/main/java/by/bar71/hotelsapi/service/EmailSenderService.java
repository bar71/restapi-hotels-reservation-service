package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.Payment;
import by.bar71.hotelsapi.domain.Reservation;

public interface EmailSenderService {
    void sendPaymentEmail(String toEmail,
                          Reservation reservation
    );
}
