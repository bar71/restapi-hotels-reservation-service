package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.HotelAdmin;

import java.util.List;

public interface HotelAdminService {

    List<HotelAdmin> selectHotelAdmins(Integer userId, Integer hotelId);

    HotelAdmin selectHotelAdminByUserId(Integer userId);

    HotelAdmin insertHotelAdmin(HotelAdmin hotelAdmin);

    void deleteHotelAdmin(Integer userId);

}
