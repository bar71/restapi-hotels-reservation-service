package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.domain.User;

import java.util.List;

public interface UserService {

    List<User> selectUsers(Integer hotelId, String email);

    User selectUserByEmail(String email);

    User selectUserByPhone(String phone);

    User selectUserById(Integer id);

    void insertUserRole(Integer userId, Integer roleId);

    void deleteUserRoles(Integer user_id);

    void insertUser(User user);

    List<User> selectAdminsOfHotel(Integer hotelId);

    void deleteUserRole(Integer userId, Integer roleId);
}
