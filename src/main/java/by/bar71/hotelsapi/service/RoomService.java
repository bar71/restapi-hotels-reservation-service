package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.Room;

import java.util.Date;
import java.util.List;

public interface RoomService {

    List<Room> selectRoom(Integer id, Integer hotelId, Integer roomClassId, Integer placesCount);

    Room selectRoomById(Integer id);

    List<Room> selectFilteredRooms(Integer hotelId, Date startDate, Date finalDate, Integer roomClassId, Integer placesCount);

    Room insertRoom(Room room);

    void updateRoom(Room room);

    void deleteRoom(Integer id);

}
