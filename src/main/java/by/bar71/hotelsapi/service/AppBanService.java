package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.ban.AppBan;
import by.bar71.hotelsapi.domain.ban.AppBanRequestDto;

import java.util.List;

public interface AppBanService {

    void insertAppBan(AppBanRequestDto appBanRequestDto, String authHeader);

    void deleteAppBan(Integer userId);

    AppBan selectAppBanByUserId(Integer userId);

    List<AppBan> selectAppBans();
}
