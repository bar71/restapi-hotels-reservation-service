package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.Role;
import by.bar71.hotelsapi.domain.Room;

import java.util.Date;
import java.util.List;

public interface RoleService {

    List<Role> selectRoles();

    Role selectRoleById(Integer id);

    Role selectRoleByName(String name);

}
