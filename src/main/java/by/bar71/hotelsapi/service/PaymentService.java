package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;

public interface PaymentService {

    List<Payment> selectPayments();

    Payment selectPaymentById(Integer id);

    Payment insertPayment(Payment payment);

    void deletePayment(Integer id);

}