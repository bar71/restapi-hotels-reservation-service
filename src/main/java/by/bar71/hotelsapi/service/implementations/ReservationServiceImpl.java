package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.*;
import by.bar71.hotelsapi.repository.ReservationRepository;
import by.bar71.hotelsapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    HotelBanService hotelBanService;

    @Autowired
    HotelService hotelService;

    @Autowired
    RoomService roomService;

    @Autowired
    EmailSenderService emailSenderService;

    @Autowired
    HotelAdminService hotelAdminService;

    @Autowired
    UserService userService;

    @Autowired
    JwtService jwtService;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    PaymentService paymentService;

    @Override
    public List<Reservation> selectReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation selectReservationById(Integer id) {
        return reservationRepository.findById(id).orElseThrow(() -> {
            throw new IllegalStateException("Reservation with passed id not found");
        });
    }

    @Override
    public List<Reservation> selectUserWaitingAcceptReservations(String authHeader) {
        User user = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        );
        return reservationRepository.findByUserAndIsAcceptedIsFalse(user);
    }

    @Override
    public List<Reservation> selectUserWaitingPaymentReservations(String authHeader) {
        User user = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        );
        return reservationRepository.findByUserAndIsAcceptedIsTrueAndIsPaidIsFalse(user);
    }

    @Override
    public List<Reservation> selectUserPaidReservations(String authHeader) {
        User user = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        );
        return reservationRepository.findByUserAndIsPaidIsTrue(user);
    }

    @Override
    public List<Reservation> selectHotelWaitingAcceptReservations(String authHeader) {
        return reservationRepository.findByHotelAndIsAcceptedIsFalse(getHotelAssociatedWithAdmin(authHeader));
    }

    @Override
    public List<Reservation> selectHotelWaitingPaymentReservations(String authHeader) {
        return reservationRepository.findByHotelAndIsAcceptedIsTrueAndIsPaidIsFalse(getHotelAssociatedWithAdmin(authHeader));
    }

    @Override
    public List<Reservation> selectHotelPaidReservations(String authHeader) {
        return reservationRepository.findByHotelAndIsPaidIsTrue(getHotelAssociatedWithAdmin(authHeader));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertReservation(InsertReservationRequestDto irrd, String authHeader) {
        Integer userId = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        ).getId();
        if (hotelBanService.selectHotelBan(
                userId,
                irrd.getHotelId()) != null) {
            throw new AccessDeniedException("Access denied");
        }

        Integer totalPrice = Math.toIntExact(DAYS.between(
                irrd.getStartDate().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate(),
                irrd.getFinalDate().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate()
        ) * roomService.selectRoomById(irrd.getRoomId()).getCostPerDay());

        irrd.setTotalPrice(totalPrice);
        irrd.setUserId(userId);
        reservationRepository.insertReservationByRequest(irrd);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void accept(Integer id, String authHeader) {
        if (!checkAdminPermission(id, authHeader)) {
            throw new AccessDeniedException("Access denied");
        }
        Reservation reservation = selectReservationById(id);
        reservation.setAccepted(true);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void reject(Integer id, String authHeader) {
        if (!checkAdminPermission(id, authHeader)) {
            throw new AccessDeniedException("Access denied");
        }
        reservationRepository.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pay(Integer id, String authHeader) {
        if (!checkClientPayPermission(id, authHeader)) {
            throw new AccessDeniedException("Access denied");
        }
        Reservation reservation = selectReservationById(id);
        reservation.setPaid(true);

        Payment payment = new Payment();
        payment.setReservation(reservation);
        payment.setCheckPrice(reservation.getTotalPrice());
        payment.setPaymentDateTime(new Date());
        paymentService.insertPayment(payment);
        reservation.setPayment(payment);
        emailSenderService.sendPaymentEmail(jwtService.extractUsernameFromAuthHeader(authHeader), reservation);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteReservation(Integer id) {
        reservationRepository.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUserNotPaidReservations(User user) {
        reservationRepository.deleteByUserAndIsPaidIsFalse(user);
    }

    boolean checkAdminPermission(Integer id, String authHeader) {
        Integer userId = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        ).getId();
        HotelAdmin hotelAdmin = hotelAdminService.selectHotelAdminByUserId(userId);
        if (hotelAdmin == null || selectReservationById(id).getHotel().getId() != hotelAdmin.getHotelId()) {
            return false;
        }
        return true;
    }

    boolean checkClientPayPermission(Integer id, String authHeader) {
        Integer userId = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        ).getId();
        Reservation reservation = selectReservationById(id);

        if (reservation == null || reservation.getUser().getId() != userId || !reservation.isAccepted()) {
            return false;
        }

        return true;
    }

    public Hotel getHotelAssociatedWithAdmin(String authHeader) {
        Integer userId = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        ).getId();
        HotelAdmin hotelAdmin = hotelAdminService.selectHotelAdminByUserId(userId);
        if (hotelAdmin == null) {
            throw new AccessDeniedException("Access denied");
        }
        return hotelService.selectHotelById(hotelAdmin.getHotelId());
    }

}
