package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.repository.CityRepository;
import by.bar71.hotelsapi.domain.City;
import by.bar71.hotelsapi.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;

    @Override
    public List<City> selectCities(Integer id, String name) {
        return cityRepository.findAll();
    }

    @Override
    public City selectCityById(Integer id) {
        Optional<City> city = cityRepository.findById(id);
        if (city == null) {
            throw new IllegalStateException("City with passed id not found");
        }
        return city.get();
    }

    @Override
    public City insertCity(String name) {
        City city = City.builder().name(name).build();
        return cityRepository.save(city);
    }

    @Override
    public void updateCity(City city) {
        Optional<City> optionalCity = cityRepository.findById(city.getId());
        if (optionalCity.isEmpty()) {
            throw new IllegalStateException("No such city found");
        }
        City c = optionalCity.get();
        c.setName(city.getName());
        cityRepository.save(c);
    }

}
