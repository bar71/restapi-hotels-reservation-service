package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.repository.AppBanRepository;
import by.bar71.hotelsapi.repository.UserRepository;
import by.bar71.hotelsapi.domain.ban.AppBan;
import by.bar71.hotelsapi.domain.ban.AppBanRequestDto;
import by.bar71.hotelsapi.service.AppBanService;
import by.bar71.hotelsapi.service.JwtService;
import by.bar71.hotelsapi.service.ReservationService;
import by.bar71.hotelsapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class AppBanServiceImpl implements AppBanService {

    @Autowired
    JwtService jwtService;

    @Autowired
    UserService userService;

    @Autowired
    AppBanRepository appBanRepository;

    @Autowired
    ReservationService reservationService;

    @Override
    public AppBan selectAppBanByUserId(Integer userId) {
        return appBanRepository.findByUserId(userId);
    }

    @Override
    public List<AppBan> selectAppBans() {
        return appBanRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertAppBan(AppBanRequestDto appBanRequestDto, String authHeader) {
        String email = jwtService.extractUsernameFromAuthHeader(authHeader);
        Integer judgeId = userService.selectUserByEmail(email).getId();
        User user = userService.selectUserByEmail(appBanRequestDto.getEmail());
        AppBan appBan = AppBan.builder()
                .banDate(new Date())
                .judge(userService.selectUserById(judgeId))
                .user(user)
                .reason(appBanRequestDto.getReason())
                .build();
        appBanRepository.save(appBan);
        reservationService.deleteUserNotPaidReservations(appBan.getUser());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAppBan(Integer userId) {
        appBanRepository.deleteByUserId(userId);
    }

}
