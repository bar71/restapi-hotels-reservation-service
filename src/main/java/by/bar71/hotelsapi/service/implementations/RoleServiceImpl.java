package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.Payment;
import by.bar71.hotelsapi.repository.RoleRepository;
import by.bar71.hotelsapi.domain.Role;
import by.bar71.hotelsapi.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public List<Role> selectRoles() {
        return roleRepository.findAll();
    }

    @Override
    public Role selectRoleById(Integer id) {
        Optional<Role> optionalRole = roleRepository.findById(id);
        if (optionalRole.isEmpty()) {
            throw new IllegalStateException("No payment found");
        }
        return optionalRole.get();
    }

    @Override
    public Role selectRoleByName(String name) {
        return roleRepository.findByName(name);
    }
}
