package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.QRoom;
import by.bar71.hotelsapi.domain.QUser;
import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.repository.RoomRepository;
import by.bar71.hotelsapi.domain.Room;
import by.bar71.hotelsapi.service.RoomService;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<Room> selectRoom(Integer id, Integer hotelId, Integer roomClassId, Integer placesCount) {
        BooleanExpression predicate = Expressions.TRUE;
        if (id != null) {
            predicate = predicate.and(QRoom.room.id.eq(id));
        }
        if (hotelId != null) {
            predicate = predicate.and(QRoom.room.hotelId.eq(hotelId));
        }
        if (roomClassId != null) {
            predicate = predicate.and(QRoom.room.roomClass.id.eq(roomClassId));
        }
        if (placesCount != null) {
            predicate = predicate.and(QRoom.room.placesCount.eq(placesCount));
        }
        return (List<Room>) roomRepository.findAll(predicate);
    }

    @Override
    public Room selectRoomById(Integer id) {
        return roomRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException("No room found");
        });
    }

    @Override
    public List<Room> selectFilteredRooms(Integer hotelId, Date startDate, Date finalDate, Integer roomClassId, Integer placesCount) {
        return roomRepository.selectFilteredRooms(hotelId, startDate, finalDate, roomClassId, placesCount);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Room insertRoom(Room room) {
        return roomRepository.save(room);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateRoom(Room room) {
        Room updateableRoom = roomRepository.findById(room.getId()).orElseThrow(() -> {
            throw new EntityNotFoundException("Room with passed id cannot be modified because it doesn't exist");
        });
        updateableRoom.setRoomClass(room.getRoomClass());
        updateableRoom.setDescription(room.getDescription());
        updateableRoom.setCostPerDay(room.getCostPerDay());
        updateableRoom.setPlacesCount(room.getPlacesCount());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRoom(Integer id) {
        roomRepository.deleteById(id);
    }


}
