package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.repository.PaymentRepository;
import by.bar71.hotelsapi.domain.*;
import by.bar71.hotelsapi.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public List<Payment> selectPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment selectPaymentById(Integer id) {
        Optional<Payment> optionalPayment = paymentRepository.findById(id);
        if (optionalPayment.isEmpty()) {
            throw new IllegalStateException("No payment found");
        }
        return optionalPayment.get();
    }

    @Override
    public Payment insertPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public void deletePayment(Integer id) {
        paymentRepository.deleteById(id);
    }
}
