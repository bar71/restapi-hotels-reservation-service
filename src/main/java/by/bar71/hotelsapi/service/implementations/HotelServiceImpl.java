package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.*;
import by.bar71.hotelsapi.domain.ban.HotelBan;
import by.bar71.hotelsapi.repository.HotelRepository;
import by.bar71.hotelsapi.service.*;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class HotelServiceImpl implements HotelService {

    @Autowired
    HotelAdminService hotelAdminService;

    @Autowired
    HotelBanService hotelBanService;

    @Autowired
    UserService userService;

    @Autowired
    JwtService jwtService;

    @Autowired
    HotelRepository hotelRepository;

    @Override
    public List<Hotel> selectHotels(Integer cityId, Integer stars) {
        BooleanExpression predicate = Expressions.TRUE;
        if (cityId != null) {
            predicate = predicate.and(QHotel.hotel.city.id.eq(cityId));
        }
        if (stars != null) {
            predicate = predicate.and(QHotel.hotel.stars.eq(stars));
        }
        return (List<Hotel>) hotelRepository.findAll(predicate);
    }

    @Override
    public Hotel selectHotelById(Integer id) {
        return hotelRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException("Hotel with passed id not found");
        });
    }

    @Override
    public List<Hotel> selectFilteredHotels(
            String cityName,
            Date startDate,
            Date finalDate,
            Integer roomClassId,
            Integer placesCount) {
        return hotelRepository.selectFilteredHotels(cityName, startDate, finalDate, roomClassId, placesCount);
    }

    @Override
    public Hotel selectHotelByAdminId(String authHeader) {
        return selectHotelById(getHotelIdAssociatedWithAdmin(authHeader));
    }

    @Override
    public List<HotelBan> selectHotelBans(String authHeader) {
        return hotelBanService.selectHotelBansByHotelId(getHotelIdAssociatedWithAdmin(authHeader));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Hotel insertHotel(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateHotel(Hotel hotel) {
        Hotel updateableHotel = hotelRepository.findById(hotel.getId()).orElseThrow(() -> {
            throw new EntityNotFoundException("Hotel with passed id cannot be modified because it doesn't exist");
        });
        updateableHotel.setDescription(hotel.getDescription());
        updateableHotel.setCity(hotel.getCity());
        updateableHotel.setAddress(hotel.getAddress());
        updateableHotel.setImage(hotel.getImage());
        updateableHotel.setDistanceToCenter(hotel.getDistanceToCenter());
        updateableHotel.setStars(hotel.getStars());
        updateableHotel.setName(hotel.getName());
    }

    public Integer getHotelIdAssociatedWithAdmin(String authHeader) {
        Integer userId = userService.selectUserByEmail(
                jwtService.extractUsernameFromAuthHeader(authHeader)
        ).getId();
        HotelAdmin hotelAdmin = hotelAdminService.selectHotelAdminByUserId(userId);
        if (hotelAdmin == null) {
            throw new AccessDeniedException("Access denied");
        }
        return hotelAdmin.getHotelId();
    }

}
