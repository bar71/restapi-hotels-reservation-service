package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.repository.HotelBanRepository;
import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.domain.HotelAdmin;
import by.bar71.hotelsapi.domain.ban.HotelBan;
import by.bar71.hotelsapi.domain.ban.HotelBanRequestDto;
import by.bar71.hotelsapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class HotelBanServiceImpl implements HotelBanService {

    @Autowired
    HotelAdminService hotelAdminService;

    @Autowired
    JwtService jwtService;

    @Autowired
    UserService userService;

    @Autowired
    HotelBanRepository hotelBanRepository;

    @Autowired
    ReservationService reservationService;

    @Override
    public HotelBan selectHotelBan(Integer userId, Integer hotelId) {
        return hotelBanRepository.findByUserIdAndHotelId(userId, hotelId);
    }

    @Override
    public List<HotelBan> selectHotelBansByHotelId(Integer hotelId) {
        return hotelBanRepository.findByHotelId(hotelId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertHotelBan(HotelBanRequestDto hotelBanRequestDto, String authHeader) {
        String email = jwtService.extractUsernameFromAuthHeader(authHeader);
        Integer judgeId = userService.selectUserByEmail(email).getId();
        HotelAdmin hotelAdmin = hotelAdminService.selectHotelAdminByUserId(judgeId);
        if (hotelAdmin == null) {
            throw new IllegalStateException("You are not an admin");
        }
        Integer hotelId = hotelAdmin.getHotelId();
        User user = userService.selectUserById(hotelBanRequestDto.getUserId());
        HotelBan hotelBan = HotelBan.builder()
                .banDate(new Date())
                .hotelId(hotelId)
                .reason(hotelBanRequestDto.getReason())
                .user(user)
                .judge(userService.selectUserById(judgeId))
                .build();
        hotelBanRepository.save(hotelBan);
        reservationService.deleteUserNotPaidReservations(user);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHotelBan(Integer userId, String authHeader) {
        String email = jwtService.extractUsernameFromAuthHeader(authHeader);
        Integer adminId = userService.selectUserByEmail(email).getId();
        HotelAdmin hotelAdmin = hotelAdminService.selectHotelAdminByUserId(adminId);
        if (hotelAdmin == null) {
            throw new AccessDeniedException("Access denied: You are not an admin");
        }
        hotelBanRepository.deleteByUserIdAndHotelId(userId, hotelAdmin.getHotelId());
    }


}
