package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.QUser;
import by.bar71.hotelsapi.repository.UserRepository;
import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.domain.User;
import by.bar71.hotelsapi.service.*;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Visitor;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;

    @Override
    public List<User> selectUsers(Integer hotelId, String email) {
        if (hotelId != null) {
            return selectAdminsOfHotel(hotelId);
        }
        BooleanExpression predicate = Expressions.TRUE;
        if (email != null) {
            predicate = predicate.and(QUser.user.email.eq(email));
        }
        return (List<User>) userRepository.findAll(predicate);
    }

    @Override
    public User selectUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User selectUserByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

    @Override
    public User selectUserById(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty()) {
            throw new IllegalStateException("No user found");
        }
        return optionalUser.get();
    }

    @Override
    public void insertUserRole(Integer userId, Integer roleId) {
        userRepository.insertUserRole(userId, roleId);
    }

    @Override
    public void deleteUserRoles(Integer userId) {
        userRepository.deleteUserRoles(userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertUser(User user) {
        userRepository.save(user);
        userRepository.insertUserRole(
                selectUserByEmail(user.getEmail()).getId(),
                roleService.selectRoleByName("CLIENT").getId()
        );
    }

    @Override
    public List<User> selectAdminsOfHotel(Integer hotelId) {
        return userRepository.selectAdminsOfHotel(hotelId);
    }

    @Override
    public void deleteUserRole(Integer userId, Integer roleId) {
        userRepository.deleteUserRole(userId, roleId);
    }

}
