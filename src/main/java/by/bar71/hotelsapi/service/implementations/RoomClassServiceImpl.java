package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.repository.RoomClassRepository;
import by.bar71.hotelsapi.domain.RoomClass;
import by.bar71.hotelsapi.service.RoomClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomClassServiceImpl implements RoomClassService {

    @Autowired
    RoomClassRepository roomClassRepository;

    @Override
    public List<RoomClass> selectAllRoomClasses() {
        return roomClassRepository.findAll();
    }

    @Override
    public RoomClass selectRoomClassById(Integer id) {
        Optional<RoomClass> optionalRoomClass = roomClassRepository.findById(id);
        if (optionalRoomClass.isEmpty()) {
            throw new IllegalStateException("RoomClass with passed id not found");
        }
        return optionalRoomClass.get();
    }
}
