package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.QHotel;
import by.bar71.hotelsapi.domain.QHotelAdmin;
import by.bar71.hotelsapi.repository.HotelAdminRepository;
import by.bar71.hotelsapi.domain.HotelAdmin;
import by.bar71.hotelsapi.service.HotelAdminService;
import by.bar71.hotelsapi.service.RoleService;
import by.bar71.hotelsapi.service.UserService;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HotelAdminServiceImpl implements HotelAdminService {

    @Autowired
    HotelAdminRepository hotelAdminRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Override
    public List<HotelAdmin> selectHotelAdmins(Integer userId, Integer hotelId) {
        BooleanExpression predicate = Expressions.TRUE;
        if (userId != null) {
            predicate = predicate.and(QHotelAdmin.hotelAdmin.userId.eq(userId));
        }
        if (hotelId != null) {
            predicate = predicate.and(QHotelAdmin.hotelAdmin.hotelId.eq(hotelId));
        }
        return (List<HotelAdmin>) hotelAdminRepository.findAll(predicate);
    }

    @Override
    public HotelAdmin selectHotelAdminByUserId(Integer userId) {
        return hotelAdminRepository.findByUserId(userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HotelAdmin insertHotelAdmin(HotelAdmin hotelAdmin) {
        HotelAdmin newHotelAdmin = hotelAdminRepository.save(hotelAdmin);
        userService.insertUserRole(hotelAdmin.getUserId(), roleService.selectRoleByName("ADMIN").getId());
        return newHotelAdmin;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHotelAdmin(Integer userId) {
        hotelAdminRepository.deleteById(userId);
        userService.deleteUserRole(userId, roleService.selectRoleByName("ADMIN").getId());
    }
}
