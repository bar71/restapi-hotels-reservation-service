package by.bar71.hotelsapi.service.implementations;

import by.bar71.hotelsapi.domain.Payment;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.service.EmailSenderService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Configuration config;

    @Value("${spring.mail.username}")
    private String fromEmail;

    private final String EMAIL_TEMPLATE = "paid-reservation.ftl";

    private final String EMAIL_SUBJECT = "Чек";

    @Override
    public void sendPaymentEmail(String toEmail,
                                Reservation reservation
    ) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Template t = config.getTemplate(EMAIL_TEMPLATE);
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, reservation);

            helper.setTo(toEmail);
            helper.setText(html, true);
            helper.setSubject(EMAIL_SUBJECT);
            helper.setFrom(fromEmail);
            mailSender.send(message);

        } catch (IOException | TemplateException | MessagingException e) {
            System.out.println(e.getMessage());
        }

    }

}
