package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.City;
import by.bar71.hotelsapi.domain.Hotel;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

public interface CityService {

    List<City> selectCities(Integer id, String name);

    City selectCityById(Integer id);

    City insertCity(String name);

    void updateCity(City city);
}
