package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.InsertReservationRequestDto;
import by.bar71.hotelsapi.domain.Reservation;
import by.bar71.hotelsapi.domain.User;

import java.util.List;

public interface ReservationService {
    List<Reservation> selectReservations();

    Reservation selectReservationById(Integer id);

    List<Reservation> selectUserWaitingAcceptReservations(String authHeader);

    List<Reservation> selectUserWaitingPaymentReservations(String authHeader);

    List<Reservation> selectUserPaidReservations(String authHeader);

    List<Reservation> selectHotelWaitingAcceptReservations(String authHeader);

    List<Reservation> selectHotelWaitingPaymentReservations(String authHeader);

    List<Reservation> selectHotelPaidReservations(String authHeader);

    void insertReservation(InsertReservationRequestDto irrd, String authHeader);

    void deleteReservation(Integer id);

    void deleteUserNotPaidReservations(User user);

    void accept(Integer id, String authHeader);

    void reject(Integer id, String authHeader);

    void pay(Integer id, String authHeader);

}
