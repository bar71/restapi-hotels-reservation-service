package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.ban.HotelBan;
import by.bar71.hotelsapi.domain.ban.HotelBanRequestDto;

import java.util.List;

public interface HotelBanService {

    void insertHotelBan(HotelBanRequestDto hotelBanRequestDto, String authHeader);

    void deleteHotelBan(Integer userId, String authHeader);

    HotelBan selectHotelBan(Integer userId, Integer hotelId);

    List<HotelBan> selectHotelBansByHotelId(Integer hotelId);
}
