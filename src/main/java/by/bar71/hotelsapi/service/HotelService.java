package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.Hotel;
import by.bar71.hotelsapi.domain.ban.HotelBan;

import java.util.Date;
import java.util.List;

public interface HotelService {

    List<Hotel> selectHotels(Integer cityId, Integer stars);

    Hotel selectHotelById(Integer id);

    List<Hotel> selectFilteredHotels(String cityName, Date startDate, Date finalDate, Integer roomClassId, Integer placesCount);

    Hotel selectHotelByAdminId(String authHeader);

    List<HotelBan> selectHotelBans(String authHeader);

    Hotel insertHotel(Hotel hotel);

    void updateHotel(Hotel hotel);

}
