package by.bar71.hotelsapi.service;

import by.bar71.hotelsapi.domain.RoomClass;

import java.util.List;

public interface RoomClassService {

    List<RoomClass> selectAllRoomClasses();

    RoomClass selectRoomClassById(Integer id);

}
