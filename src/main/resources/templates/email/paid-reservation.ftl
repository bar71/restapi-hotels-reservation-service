<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HOTELS71</title>
    <style>
        body {
            font-family: "Roboto Light";
        }
        hr {
            color: black;
            background-color: black;
        }
        .reservation {
            background-color: bisque;
            border: solid 2px black;
            font-size: 18px;
            padding: 20px;
        }
    </style>
</head>
<body>
    <div class="main">
        <h2>Дорогой ${user.name}, оплата проведена успешно!</h2>

        <div class="reservation">

            <div class="hotel">
                <div>
                    Отель: ${hotel.name}
                </div>
                <div>
                    Адрес: г. ${hotel.city.name}, ${hotel.address}
                </div>
            </div>

            <hr/>

            <div class="room">
                <div>
                    Комната: ${room.description}
                </div>
                <div>
                    Класс комнаты: ${room.roomClass.name}
                </div>
                <div>
                    Время пребывания: c <strong> ${startDate?date}</strong> по <strong>${finalDate?date}</strong>
                </div>
            </div>

            <hr/>

            <div class="payment">
                <div>
                    Сумма оплаты: <span>$ ${payment.checkPrice}</span>
                </div>
                <div>
                    Дата и время оплаты: <span>${payment.paymentDateTime?datetime}</span>
                </div>
            </div>

        </div>

        <footer>
            <h3>Благодарим за пользование сервисом HOTELS71</h3>
        </footer>
    </div>
</div>
</body>
</html>